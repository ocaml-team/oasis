Source: oasis
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Build-Depends:
  debhelper-compat (= 13),
  dh-ocaml,
  ocaml,
  ocaml-best-compilers,
  ocaml-findlib (>= 1.3.1),
  ocamlbuild,
  ocamlify,
  ocamlmod (>= 0.0.7),
  libounit-ocaml-dev (>= 2.0.0),
  libfindlib-ocaml-dev,
  libfileutils-ocaml-dev (>= 0.4.2),
  libcamlp-streams-ocaml-dev,
  zlib1g-dev,
  libidn-dev
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: http://oasis.forge.ocamlcore.org/
Vcs-Git: https://salsa.debian.org/ocaml-team/oasis.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/oasis

Package: oasis
Architecture: any
Suggests: liboasis-ocaml-doc
Depends:
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends},
 liboasis-ocaml-dev
Description: Build-system generation for OCaml projects -- binaries
 OASIS generates a full configure, build and install system for your
 application. It starts with a simple `_oasis` file at the toplevel of your
 project and creates everything required.
 .
 It uses external tools like OCamlbuild and it can be considered as the glue
 between various subsystems that do the job. It should support the following
 tools:
 .
  - OCamlbuild
  - OMake (todo)
  - OCamlMakefile (todo),
  - ocaml-autoconf (todo)
 .
 It also features a do-it-yourself command line invocation and an internal
 configure/install scheme. Libraries are managed through findlib. It has been
 tested on GNU Linux and Windows.
 .
 OASIS supports standard entry points and descriptions. It helps to
 integrates your libraries and software with third parties tools like GODI.
 .
 This package contains command-line tools.

Package: liboasis-ocaml-dev
Architecture: any
Suggests: liboasis-ocaml-doc
Depends:
 ocamlbuild,
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Recommends: ocaml-findlib
Description: Build-system generation for OCaml projects -- development files
 OASIS generates a full configure, build and install system for your
 application. It starts with a simple `_oasis` file at the toplevel of your
 project and creates everything required.
 .
 It uses external tools like OCamlbuild and it can be considered as the glue
 between various subsystems that do the job. It should support the following
 tools:
 .
  - OCamlbuild
  - OMake (todo)
  - OCamlMakefile (todo),
  - ocaml-autoconf (todo)
 .
 It also features a do-it-yourself command line invocation and an internal
 configure/install scheme. Libraries are managed through findlib. It has been
 tested on GNU Linux and Windows.
 .
 OASIS supports standard entry points and descriptions. It helps to
 integrates your libraries and software with third parties tools like GODI.
 .
 This package contains the development files needed for using OASIS as
 a library.

Package: liboasis-ocaml
Architecture: any
Suggests: liboasis-ocaml-doc
Depends:
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Description: Build-system generation for OCaml projects -- runtime
 OASIS generates a full configure, build and install system for your
 application. It starts with a simple `_oasis` file at the toplevel of your
 project and creates everything required.
 .
 It uses external tools like OCamlbuild and it can be considered as the glue
 between various subsystems that do the job. It should support the following
 tools:
 .
  - OCamlbuild
  - OMake (todo)
  - OCamlMakefile (todo),
  - ocaml-autoconf (todo)
 .
 It also features a do-it-yourself command line invocation and an internal
 configure/install scheme. Libraries are managed through findlib. It has been
 tested on GNU Linux and Windows.
 .
 OASIS supports standard entry points and descriptions. It helps to
 integrates your libraries and software with third parties tools like GODI.
 .
 This package contains the shared runtime libraries and plugins.

Package: liboasis-ocaml-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Multi-Arch: foreign
Description: Architecture for building OCaml libraries and applications
 OASIS generates a full configure, build and install system for your
 application. It starts with a simple `_oasis` file at the toplevel of your
 project and creates everything required.
 .
 It uses external tools like OCamlbuild and it can be considered as the glue
 between various subsystems that do the job. It should support the following
 tools:
 .
  - OCamlbuild
  - OMake (todo)
  - OCamlMakefile (todo),
  - ocaml-autoconf (todo)
 .
 It also features a do-it-yourself command line invocation and an internal
 configure/install scheme. Libraries are managed through findlib. It has been
 tested on GNU Linux and Windows.
 .
 OASIS supports standard entry points and descriptions. It helps to
 integrates your libraries and software with third parties tools like GODI.
 .
 This package contains the documentation.
